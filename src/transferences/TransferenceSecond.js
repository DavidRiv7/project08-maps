var moment = require( 'moment' );
moment.locale( 'es' );

import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    TextInput,
} from 'react-native'

export default class TransferenceSecond extends Component {
    constructor ( props ) {
        super( props );
    }
    render() {
        var params = this.props.route.params;
        return (
            <View style={ styles.imgFondo } >
                <View style={ styles.difuminado }>
                    {/*<View style={ styles.head }>
                        <Text style={ styles.headText }>Confirmar</Text>
                    </View> */}
                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>Cuenta origen:</Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.origen }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>
                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>Cuenta destino: </Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.destino }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>
                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>Importe: </Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.impor }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>
                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>Referencia: </Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.refe }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>

                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>Fecha: </Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.fech }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>

                    <View style={ styles.box }>
                        <Text style={ styles.textStyle }>¿Notificar? </Text>
                        <TextInput
                            style={ styles.input }
                            value={ params.notif }
                            placeholderTextColor={ '#222' }
                            editable={ false }
                        />
                    </View>

                    <TouchableOpacity style={ styles.boton }
                        onPress={ () => this.props.navigation.navigate( 'Hecho' ) }>
                        <Text style={ { color: '#fff', fontSize: 22 } }>Confirmar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={ styles.botonCancel }
                        onPress={ () => this.props.navigation.navigate( 'Formulario' ) } >
                        <Text style={ { color: '#fff', fontSize: 22 } }> Volver </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create( {
    imgFondo: {
        flex: 1,
        width: null, height: null,
        margin: 0, padding: 0,
        resizeMode: "contain",
    },
    difuminado: {
        flex: 1,
        padding: '4%', paddingTop: '6%', margin: 0,
        backgroundColor: 'rgba(0,0,0,0.8)'
    },
    head: {
        margin: '4%'
    },
    headText: {
        flex: 0, alignSelf: "center",
        fontSize: 40, color: 'yellow',
        fontWeight: 'bold', textTransform: 'uppercase',
    },
    box: {
        flex: 0, padding: '1%',
    },
    input: {
        backgroundColor: 'rgba(180,180,180,0.8)',
        width: null, borderColor: 'gray', padding: '2%',
        borderWidth: 1, borderRadius: 20, fontSize: 18,
        color: '#000'
    },
    textStyle: {
        fontSize: 20, color: '#fff', marginLeft: '2%', textTransform: 'capitalize'
    },
    button: {
        alignItems: 'center',
        padding: '2%',
        width: null,
        marginBottom: '6%',
    },
    boton: {
        alignSelf: 'center', padding: '3%', margin: '4%',
        backgroundColor: 'rgba(0, 165, 199, 1)', borderRadius: 20,
    },
    botonCancel: {
        alignSelf: 'center', padding: '3%', margin: '1%',
        backgroundColor: 'rgba(200,0,20, 1)', borderRadius: 20,
    },
    extraText: {
        fontSize: 18,
        color: 'yellow', textAlign: 'center',
    },
} );