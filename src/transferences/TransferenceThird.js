var moment = require( 'moment' );
moment.locale( 'es' );

import React, { Component } from 'react';
import {
    Text, View, TouchableOpacity, StyleSheet, Image
} from 'react-native';
const img = require( '../../img/done.png' );

export default class TransferenceThird extends Component {
    constructor ( props ) {
        super( props );
        this.state = {  }
    }
    render() {
        return (
            <View style={ styles.container }>
                <Text style={styles.headText}>¡Transferencia completada!</Text>
                <Image style={ styles.tinyLogo }
                    source={img } />
                <TouchableOpacity style={ styles.boton }
                    onPress={ () => this.props.navigation.navigate( 'Formulario' ) }>
                    <Text style={styles.botonText}>Aceptar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        width: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor:'rgba(0,0,0,0.8)',
        padding:'4%',
    },
    headText:{
        color: '#fff', textAlign: 'center', 
        fontSize:40, fontWeight:'bold', textTransform: 'uppercase',
    },
    tinyLogo: {
        width: 260,
        height: 200,
        alignSelf: 'center',
        margin: '4%',
        alignSelf:'center',
    },
    boton: {
        alignSelf:'center',backgroundColor: 'rgba(0, 165, 199, 1)',
        margin: '4%', padding: '2%',  borderRadius: 20,
    },
    botonText:{
        textAlign: 'center',
        fontSize: 26, color:'#fff', textTransform: 'capitalize',
    },
} );