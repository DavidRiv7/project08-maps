var moment = require( 'moment' );
moment.locale( 'es' );

import React, { Component } from 'react';
import {
    Text, View, ScrollView, TouchableOpacity, StyleSheet, TextInput, Switch
} from 'react-native';
import DateTime from 'react-native-modal-datetime-picker';
import ModalSelector from 'react-native-modal-selector';

var day = new Date().getDate();
var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();
var actualDate = `${ day }-${ month }-${ year }`;

var index = 0;
const data = [
    {
        key: index++, cuenta: '000000123455',
        component: <View ><Text style={ { color: 'black' } }>000000123455</Text></View>
    },
    {
        key: index++, cuenta: '000000123456',
        component: <View ><Text style={ { color: 'black' } }>000000123456</Text></View>
    },
    {
        key: index++, cuenta: '000000123457',
        component: <View ><Text style={ { color: 'black' } }>000000123457</Text></View>
    },
    {
        key: index++, cuenta: '000000123458',
        component: <View ><Text style={ { color: 'black' } }>000000123458</Text></View>
    },
    {
        key: index++, cuenta: '000000123459',
        component: <View ><Text style={ { color: 'black' } }>000000123459</Text></View>
    }
];

export default class TransferenceFirst extends Component {
    constructor ( props ) {
        super( props );
        this.state = {
            c_origen: '',
            c_destino: '',
            importe: '',
            referencia: '',
            fecha: `${ actualDate }`,
            notificar: 'No',
            isDateTimePickerVisible: false,
            isEnabled: false,
        }
    }

    showDateTimePicker = () => this.setState( { isDateTimePickerVisible: true } );
    hideDateTimePicker = () => this.setState( { isDateTimePickerVisible: false } );
    handleDatePicked = ( date ) => {
        this.hideDateTimePicker();
        this.setState( {
            fecha: moment( date ).format( 'DD/MM/YYYY' )
        } );
    };

    EvaluarSwitch = () => {
        if ( this.state.isEnabled === true ) {
            this.setState( { notificar: 'Sí' } );
        } else if ( this.state.isEnabled === false ) {
            this.setState( { notificar: 'No' } );
        } else {
            alert( 'Error de variable booleana!' )
        }
    }

    validarCampos = () => {
        if ( this.state.c_origen == '' || this.state.c_destino == '' || this.state.importe == ''
            || this.state.referencia == '' || this.state.notificar == '' ) {

            alert( 'Llene todos los campos.' );

        } else {
            if ( this.state.c_origen == this.state.c_destino ) {
                alert( 'No puede realizar esta operación en una misma cuenta.' );
            } else {
                this.EvaluarSwitch;
                this.props.navigation.navigate( 'Confirmar', {
                    origen: this.state.c_origen, destino: this.state.c_destino,
                    impor: this.state.importe, refe: this.state.referencia,
                    fech: this.state.fecha, notif: this.state.notificar
                } );
            }
        }
    }

    render() {

        const DatePicker = () => (
            <View style={ { flex: 1 } }>
                <Text style={ styles.textM }>Seleccione una fecha:</Text>
                <TouchableOpacity style={ styles.boton } onPress={ this.showDateTimePicker }>
                    <Text style={ { color: '#fff', fontSize: 18 } }>{ this.state.fecha }</Text>
                </TouchableOpacity>
                <DateTime
                    isVisible={ this.state.isDateTimePickerVisible }
                    onConfirm={ this.handleDatePicked }
                    onCancel={ this.hideDateTimePicker }
                    mode={ 'date' }
                    isDarkModeEnabled={ true }
                />
            </View>
        );

        return (
            <View style={ styles.container }>
                <ScrollView style={ { flex: 0 } } >
                    {/*<Text style={ {
                        fontSize: 40, textAlign: 'center', color: 'yellow',
                        textTransform: 'uppercase', fontWeight: 'bold', padding: '4%'
                    } }> Transferencia </Text>*/}
                    <Text style={ styles.textM }>Cuenta origen:</Text>
                    <ModalSelector
                        data={ data }
                        initValue="Seleccione una cuenta!"
                        supportedOrientations={ [ 'landscape' ] }
                        accessible={ true }
                        scrollViewAccessibilityLabel={ 'Scrollable options' }
                        cancelButtonAccessibilityLabel={ 'Cancel Button' }
                        onChange={ ( option ) => { this.setState( { c_origen: option.cuenta } ) } }>
                        <TextInput
                            style={ styles.input }
                            editable={ false }
                            placeholder="Seleccione una cuenta!"
                            placeholderTextColor='#333'
                            value={ this.state.c_origen } />
                    </ModalSelector>

                    <Text style={ styles.textM }>Cuenta destino:</Text>
                    <ModalSelector
                        data={ data }
                        initValue="Seleccione una cuenta!"
                        supportedOrientations={ [ 'landscape' ] }
                        accessible={ true }
                        scrollViewAccessibilityLabel={ 'Scrollable options' }
                        cancelButtonAccessibilityLabel={ 'Cancel Button' }
                        onChange={ ( option ) => { this.setState( { c_destino: option.cuenta } ) } }>
                        <TextInput
                            style={ styles.input }
                            editable={ false }
                            placeholder="Seleccione una cuenta!"
                            placeholderTextColor='#333'
                            value={ this.state.c_destino } />
                    </ModalSelector>

                    <Text style={ styles.textM }>Importe:</Text>
                    <TextInput style={ styles.input } placeholder={ 'Una cantidad valida' }
                        maxLength={ 5 } keyboardType={ 'numeric' } placeholderTextColor='#999'
                        value={ this.state.importe }
                        onChangeText={
                            ( numb ) => this.setState( {
                                importe: numb.replace( /[^0-9]/g, '' )
                            } )
                        } />

                    <Text style={ styles.textM }>Referencia:</Text>
                    <TextInput style={ styles.input } placeholder={ 'Escribe algo' } placeholderTextColor='#999'
                        onChangeText={ ( text ) => this.setState( {
                            referencia: text.replace( /[^A-Za-z]/g, '' )
                        } ) }
                        value={ this.state.referencia } />

                    <DatePicker />

                    <View style={ { alignSelf: 'center', flex: 1 } }>
                        <Text style={ styles.textM }>
                            ¿Notificar? { this.state.isEnabled ? 'Sí' : 'No' }
                        </Text>
                        <Switch
                            trackColor={ { false: "#767577", true: "#81b0ff" } }
                            thumbColor={ this.state.isEnabled ? "#f5dd4b" : "#f4f3f4" }
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={ ( isEnabled ) => this.setState( {
                                isEnabled, notificar: this.state.notificar ? 'Sí' : 'No'
                            } ) }
                            style={ { alignSelf: 'center' } }
                            value={ this.state.isEnabled } />
                    </View>

                    <TouchableOpacity style={ styles.boton }
                        onPress={ ( this.validarCampos ) }>
                        <Text style={ {
                            color: '#fff', textTransform: 'uppercase'
                        } } > Continuar </Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>

        )
    }
}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(50,50,50,1)',
        width: '100%', padding: '8%', height: '100%',
    },
    boton: {
        backgroundColor: 'rgba(0, 165, 199, 1)',
        margin: '4%', padding: '4%',
        alignSelf: 'center', borderRadius: 40,
    },
    placeholderText: {
        color: '#fff',
    },
    textM: {
        marginTop: '2%', width: '100%',
        fontSize: 20, color: '#fff', marginLeft: '2%',
    },
    mItem: {
        backgroundColor: 'gray',
    },
    input: {
        backgroundColor: 'rgba(180,180,180,0.8)',
        width: null, borderColor: 'gray', padding: '2%',
        borderWidth: 1, borderRadius: 20, fontSize: 16,
        color: '#000'
    },
} );