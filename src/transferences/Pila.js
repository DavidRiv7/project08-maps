import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Vistas
import First from './TransferenceFirst';
import Second from './TransferenceSecond';
import Third from './TransferenceThird';

function Banco() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator >
            <Stack.Screen name='Formulario' component={ First }
                options={ {
                    headerStyle: {
                        backgroundColor: 'rgba(24, 97, 190, 1)', 
                    },
                    headerTintColor: 'yellow',
                    headerBackTitleVisible: false,
                    headerTitleStyle: {
                        fontWeight: 'bold', alignSelf: 'center', textTransform: 'uppercase',
                    },
                } } />
            <Stack.Screen name='Confirmar' component={ Second }
                options={ {
                    headerStyle: {
                        backgroundColor: 'rgba(24, 97, 190, 1)', 
                    },
                    headerTintColor: 'yellow',
                    headerBackTitleVisible: false,
                    headerTitleStyle: {
                        fontWeight: 'bold', alignSelf: 'center', textTransform: 'uppercase',
                    },
                } } />
            <Stack.Screen name='Hecho' component={ Third }
                options={ { headerBackTitleVisible: false, headerShown: false} } />
                
        </Stack.Navigator>
    )
}

export default class Pilas extends Component {

    constructor ( props ) {
        super( props );
        this.state = {}
    }

    render() {
        return (
            <Banco />
        )
    }
}