import React, { Component } from 'react';
import {
    View, Text, StyleSheet, FlatList, TouchableOpacity, Image, ImageBackground
} from 'react-native';
import data from '../data/restaurantes.json';
const cardBack = require( '../../../img/food_fondo.png' );
const imgFondo = 'https://i.pinimg.com/originals/c9/0c/28/c90c286782c791e97aabfe084c6ed0b7.jpg';

export default class Restaurant extends Component {

    constructor ( props ) {
        super( props );
    }

    Items( { items, navigation } ) {
        return (
            <TouchableOpacity
                onPress={ () => navigation.navigate( 'Detail', {
                    nombre: items.name, descripcion: items.descripcion,
                    img: items.img, coordenadas: items.coordinates,
                    id: items.key,
                } ) }  >

                <ImageBackground style={ styles.card } /*source={ { uri: cardImg } }*/
                    source={ cardBack }>
                    <View style={ {
                        flex: 1, flexDirection: 'row', height: '100%', width: '100%',
                        backgroundColor: 'rgba(0,0,0,0.36)'
                    } } >
                        <Image source={ { uri: items.img } } style={ styles.images } />
                        <View style={ { marginHorizontal: '2%', padding: '2%', } } >
                            <Text style={ styles.title }>{ items.name }</Text>
                            <Text style={ styles.resumen }>{ items.resumen }</Text>
                        </View>
                    </View>
                </ImageBackground>

            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={ styles.container }>
                <ImageBackground style={ styles.imgFondo }
                    source={ { uri: imgFondo} } >
                    <FlatList
                        data={ data.places }
                        renderItem={ ( { item } ) => <this.Items navigation={ this.props.navigation } items={ item } /> }
                        keyExtractor={ item => item.id }
                    />

                </ImageBackground>
            </View>
        );
    }

}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    imgFondo: {
        flex: 1, resizeMode: 'cover', width: null, padding: '4%',
    },
    card: {
        flex: 1,
        alignSelf: 'center', width: '100%',
        backgroundColor: 'rgba(255, 60, 40, 0.7)',
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 10, borderColor: '#fff',
    },
    images: {
        flexDirection: 'column',
        width: 60,
        height: 80,
    },
    title: {
        fontSize: 22, color: 'rgba(255,255,25,0.9)',
        flexDirection: 'row',
        fontWeight: 'bold',
        textAlign: 'left',
    },
    resumen: {
        flexDirection: 'row', color: 'rgba(255,255,255,1)'
    },
} );