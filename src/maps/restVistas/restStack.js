import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ListaRestaurantes from './Restaurant';
import DetailsRestaurantes from './DetailsRestaurant';
import Ubications from './MapsRest';

function Resturantes() {
    const Stack = createStackNavigator();

    return (
        <Stack.Navigator >
            <Stack.Screen name='Lista' component={ ListaRestaurantes }
                options={ {
                    title: 'Restaurantes',
                    headerStyle: {
                        backgroundColor: 'rgba(24, 97, 190, 1)',
                    },
                    headerTintColor: 'yellow',
                    headerShown: false,
                    headerBackTitleVisible: false,
                    headerTitleStyle: {
                        fontWeight: 'bold', alignSelf: 'center', textTransform: 'uppercase',
                    },
                } } />

            <Stack.Screen name='Detail' component={ DetailsRestaurantes }
                options={ {
                    title: 'Detalles del Restaurante',
                    headerStyle: {
                        backgroundColor: 'rgba(24, 97, 190, 1)',
                    },
                    headerTintColor: 'yellow',
                    headerShown:false,
                    headerBackTitleVisible: false,
                    headerTitleStyle: {
                        fontWeight: 'bold', alignSelf: 'center', textTransform: 'uppercase',
                    },
                } } />

            <Stack.Screen name='Mapa' component={ Ubications }
                options={ {
                    titlle: 'Ubicación',
                    headerStyle: {
                        backgroundColor: 'rgba(24, 97, 190, 1)',
                    },
                    headerTintColor: 'yellow',
                    headerTitleStyle: {
                        fontWeight: 'bold', alignSelf: 'center', textTransform: 'uppercase',
                    },
                } } />

        </Stack.Navigator>
    );
}

export default class restStack extends Component {
    constructor ( props ) {
        super( props );
    }
    render() {
        return (
            <Resturantes />
        )
    }

}