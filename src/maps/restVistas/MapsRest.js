import React, { Component } from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';

export default class MapsRest extends Component {
    constructor ( props ) {
        super( props );
    }

    render() {
        var params = this.props.route.params;
        return (
            <View style={ styles.container }>
                <MapView
                    style={ styles.map }
                    provider={ PROVIDER_GOOGLE }
                    initialRregion={ {
                        latitude: -16.398889,
                        longitude: -71.535003,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    } } >
                    <Marker
                        key={ params.id }
                        coordinate={ {
                            latitude: params.latitud,
                            longitude: params.longitud,
                        } }>
                        <Callout>
                            <View style={ { backgroundColor: '#FFF', borderRadius: 5, } } >
                                <Text>
                                    { params.nombre }
                                </Text>
                            </View>
                        </Callout>
                    </Marker>
                </MapView>
            </View>
        );
    }
}
const styles = StyleSheet.create( {
    container: {
        flex: 1,
        width: null,
        minHeight: '100%',
    },
    map: {
        minHeight: '60%',
        height: '100%',
    },
} );