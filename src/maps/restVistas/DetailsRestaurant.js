import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ScrollView, Image, ImageBackground
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const imgFondo = 'https://i.pinimg.com/originals/c9/0c/28/c90c286782c791e97aabfe084c6ed0b7.jpg';
const back2 = require( '../../../img/back2.jpg' );
export default class DetailsRestaurant extends Component {
    constructor ( props ) {
        super( props );
        this.state = {}
    }

    render() {
        var params = this.props.route.params;
        return (
            <View style={ styles.container } >
                <ImageBackground source={ { uri: imgFondo } } style={ styles.imgBack }>
                    <ScrollView style={ styles.difuminado }>
                        <Text style={ styles.textM }> { params.nombre } </Text>
                        <Image style={ styles.image } source={ { uri: params.img } } />
                        <View style={ { alignSelf: 'center', padding: '2%' } }>
                            <Text style={ styles.descip }> { params.descripcion } </Text>
                        </View>
                        <TouchableOpacity
                            style={ styles.boton }
                            onPress={ () => this.props.navigation.navigate( 'Mapa' ,{
                                latitud:params.coordenadas.latitude, 
                                longitud: params.coordenadas.longitude,
                                nombre: params.nombre,
                                id: params.id,
                            }) }>
                            <Text>Ver ubicación</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={ styles.boton }
                            onPress={ () => this.props.navigation.navigate( 'Lista' ) }>
                            <Text>Volver</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </ImageBackground>
            </View >
        );
    }
}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: 'rgba(0,0,0, 1)',
    },
    difuminado: {
        backgroundColor: 'rgba(0,0,0, 0.5)', padding: '2%',
    },
    imgBack: {
        flex: 1, resizeMode: 'cover', width: null, padding: '4%',
    },
    textM: {
        textAlign: 'center',
        fontSize: 34, padding: '4%', fontWeight: 'bold',
        color: 'rgba(255,255,25,0.9)',
    },
    image: {
        alignSelf: 'center',
        borderWidth: 1, borderColor: '#fff',
        height: 300, width: '80%',
    },
    descip: {
        color: 'rgba(255,255,255, 1)', fontSize: 24,
        textAlign: 'center',
    },
    boton: {
        backgroundColor: 'rgba(200,200,200,1)',
        padding: '3%', alignSelf: 'center',
    },
} );