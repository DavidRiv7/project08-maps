import React, { Component } from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';

export default class Maps extends Component {
    constructor ( props ) {
        super( props );
        this.state = {
            places: [
                {
                    name: 'Tecsup Norte',
                    coordinates: {
                        latitude: -8.1494049,
                        longitude: -79.0437666,
                    },
                },
                {
                    name: 'Tecsup Centro',
                    coordinates: {
                        latitude: -12.0447884,
                        longitude: -76.954511,
                    },
                },
                {
                    name: 'Tecsup Sur',
                    coordinates: {
                        latitude: -16.4288508,
                        longitude: -71.5038502,
                    },
                },
            ],
        };
    }

    render() {
        return (
            <View style={ styles.container }>
                <MapView
                    style={ styles.map }
                    provider={ PROVIDER_GOOGLE }
                    initialRegion={ {
                        latitude: -16.409046,
                        longitude: -71.537453,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    } } >
                    { this.state.places.map( ( item, index ) => {
                        return (
                            <Marker
                                key={ index }
                                coordinate={ {
                                    latitude: item.coordinates.latitude,
                                    longitude: item.coordinates.longitude,
                                } }>
                                <Callout>
                                    <View
                                        style={ { backgroundColor: '#FFF', borderRadius: 5, } }>
                                        <Text>
                                            { item.name }
                                        </Text>
                                    </View>
                                </Callout>
                            </Marker>
                        );
                    } ) }
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create( {
    container: {
        flex: 1,
        width: null, height: '100%',
    },
    map:{
        minHeight: '60%',
        height: '100%',
    }
} );