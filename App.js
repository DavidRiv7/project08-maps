import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MapsScreen from './src/maps/Maps';
import IconFoAw from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMI from 'react-native-vector-icons/MaterialIcons';
// Stacks
import Transferences from './src/transferences/Pila';
import Restaurant from './src/maps/restVistas/restStack';

function Maps() {
  return (
    <View>
      <Text>Hola Maps</Text>
    </View>
  );
}

function TabNav() {
  //const Tab = createBottomTabNavigator();
  const Tab = createMaterialBottomTabNavigator();
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#f0edf6"
        inactiveColor="rgba(0, 69, 158, 1)"
        barStyle={{ backgroundColor: 'rgba(24, 97, 190, 1)' }}>

        <Tab.Screen
          name="Bank"
          component={Transferences}
          options={{
            tabBarLabel: 'Banca',
            tabBarIcon: ({color}) => (
              <IconFoAw name="bank" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Maps"
          component={MapsScreen}
          options={{
            tabBarLabel: 'Location',
            tabBarIcon: ({color}) => (
              <IconFoAw name="google-maps" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Restaurantes"
          component={Restaurant}
          options={{
            tabBarLabel: 'Restaurantes',
            tabBarIcon: ({color}) => (
              <IconMI name="restaurant" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estado: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TabNav />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: '#dad',
    padding: '2%',
    margin: '2%',
  },
});
